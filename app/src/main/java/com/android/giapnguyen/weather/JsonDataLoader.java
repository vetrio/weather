package com.android.giapnguyen.weather;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class JsonDataLoader extends AsyncTask<String, Void, String> {

    private Context context;
    private String  location;
    private StringBuilder query;

    public JsonDataLoader(Context context) {
        this.context = context;
        this.query = new StringBuilder();
        query.append("https://query.yahooapis.com/v1/public/yql?q=");
        query.append("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"");
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... params) {
        query.append(params[0]).append("\")&format=json");
        String address = query.toString().replace(" ", "%20");

        HttpURLConnection urlConnection = null;
        String result = "";
        try {
            URL url = new URL(address);
            urlConnection = (HttpURLConnection) url.openConnection();
            int code = urlConnection.getResponseCode();
            if(code==200){
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                if (in != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                    String line = "";

                    while ((line = bufferedReader.readLine()) != null)
                        result += line;
                }
                in.close();
            }

            return result;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
            urlConnection.disconnect();
        }
        return result;

    }

    @Override
    protected void onPostExecute(String result) {
        //setDataFromResult(result);
        Log.d("aaaaaaaaaaaaaa", result);
        super.onPostExecute(result);
    }

    public void setDataFromResult(String result) throws JSONException {
        List<Contact> contactList = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(result);
        JSONArray jsonArray = jsonObject.getJSONArray("contacts");
        for (int i = 0; i < jsonArray.length(); i++) {
            String obj = jsonArray.getString(i);
            Gson gson = new GsonBuilder().create();
            Contact contact = gson.fromJson(obj, Contact.class);
            contactList.add(contact);
        }
        if(!contactList.isEmpty()) mListView.setAdapter(new ContactAdapter(mContext, contactList));
    }
}
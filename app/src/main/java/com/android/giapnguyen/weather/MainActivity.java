package com.android.giapnguyen.weather;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{

    private RecyclerView weatherRecycler;
    private WeatherRecyclerAdapter weatherAdapter;
    private ArrayList<Weather> weathers;
    private JsonDataLoader jsonDataLoader;
    private ImageButton locationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        locationButton = (ImageButton) findViewById(R.id.location_button);
        weatherRecycler = (RecyclerView) findViewById(R.id.next_recycler);
        weatherRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        weathers = new ArrayList<>();

        Weather weather;
        for(int i = 0; i < 10; i++){
            weather = new Weather("Sun", R.drawable.cloudy, "20/27");
            weathers.add(weather);
        }
        weatherAdapter = new WeatherRecyclerAdapter(getApplicationContext(), weathers);
        weatherRecycler.setAdapter(weatherAdapter);

        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jsonDataLoader = new JsonDataLoader(getApplicationContext());
                jsonDataLoader.execute("hanoi vietnam");
            }
        });


    }
}

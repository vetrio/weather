package com.android.giapnguyen.weather;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class WeatherRecyclerAdapter extends RecyclerView.Adapter<WeatherRecyclerAdapter.ViewHolder>{

    private ArrayList<Weather> weathers;
    private Context context;

    public WeatherRecyclerAdapter(Context context, ArrayList<Weather> weathers) {
        this.context = context;
        this.weathers = weathers;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_weather, parent, false);
        ViewHolder viewHolder = new ViewHolder(convertView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.name.setText(weathers.get(position).getName());
        holder.cencius.setText(weathers.get(position).getCencius());
        holder.icon.setImageResource(weathers.get(position).getImageId());
    }

    @Override
    public int getItemCount() {
        return weathers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView name, cencius;
        private ImageView icon;

        public ViewHolder(View convertView) {
            super(convertView);
            name = (TextView) convertView.findViewById(R.id.next_name);
            icon = (ImageView) convertView.findViewById(R.id.next_icon);
            cencius = (TextView) convertView.findViewById(R.id.next_cencius);
        }
    }
}
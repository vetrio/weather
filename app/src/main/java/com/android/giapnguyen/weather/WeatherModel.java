package com.android.giapnguyen.weather;

/**
 * Created by sev_user on 8/6/2016.
 */
public class WeatherModel {
    private Location location;






    public class Location {
        public String city;
        public String country;
        public String region;
    }
    public class Wind {
        public String chill;
        public String direction;
        public String speed;
    }
    public class Atmosphere {
        public String humidity;
        public String pressure;
        public String rising;
        public String visibility;
    }
    public class Astronomy {
        public String sunrise;
        public String sunset;
    }
    public class Item {
        public String sunrise;
        public String sunset;
    }
    public class Forecast{


    }
}


package com.android.giapnguyen.weather;

/**
 * Created by sev_user on 8/6/2016.
 */
public class Weather {
    private String name;
    private int imageId;
    private String cencius;

    public Weather(String name, int imageId, String cencius) {
        this.name = name;
        this.imageId = imageId;
        this.cencius = cencius;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getCencius() {
        return cencius;
    }

    public void setCencius(String cencius) {
        this.cencius = cencius;
    }
}
